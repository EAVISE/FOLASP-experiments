#! /bin/bash
basedir=$(dirname $0)
$basedir/clingo <(echo "$1") $2 $3 2> /dev/null | grep costsum | awk -F"[,()]" '{print $2}'
exit ${PIPESTATUS[0]}