#! /bin/bash
read answerRow

if [ "$answerRow" == "INCONSISTENT" ] && [ $1 != 20 ]; then
	echo FAIL
	echo MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT >&2
	exit 1
fi
if [ "$answerRow" != "INCONSISTENT" ] && [ $1 == 20 ]; then
	echo FAIL
	echo MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT >&2
	exit 1
fi
if [ "$answerRow" != "INCOMPLETE" ] && [ $1 != 10 -a $1 != 20 -a $1 != 30 ] ; then
	echo FAIL
	echo MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT >&2
	exit 1
fi
if [ $1 == 20 ]; then
	echo DONTKNOW
        echo Instances with no solution should not be checked with this checker >&2
        exit 2
else
	if [ $1 != 10 ] && [ $1 != 30 ] ; then
		echo FAIL
	        exit 1
	else
		# Filter for output predicates valve/2, everything else will be ignored
		#echo "$answerRow" | grep -o -e 'valve([^,^)]\+,[^)^,]\+)\.' | ./clingo constraint.asp $2 -- >/dev/null 2>&1
		basedir=$(dirname $0)
# 		echo "$answerRow" | cat $basedir/constraint.asp $2 - | $basedir/clingo - #| grep costsum | awk -F"[,()]" '{print $2}'
		systemresult=`$basedir/check_solution.sh "$answerRow" $basedir/constraint.asp $2`
# 		exit_clingo=${PIPESTATUS[2]}
		exit_clingo=$?
		if [ $exit_clingo == 10 ]; then
			echo OK $systemresult
			exit 0

		else
			if [ $exit_clingo == 20 ]; then
				echo FAIL
				exit 1
			else
	    			echo WARN
	    			echo Unrecognized input stream or general checker failure. >&2
	    			exit 1
			fi
		fi
	fi
fi
