symm_pipe(A,B) :- pipe(A,B).
symm_pipe(B,A) :- pipe(A,B).
%Adjacency of pipes (common junction and unshared junctions)
adj(pipe(X,Y), pipe(W,Z), COM, U1, U2) :- symm_pipe(COM,U1), symm_pipe(COM,U2), U1!=U2, not tank(COM),
				pipe(X,Y), pipe(W,Z), 2 {COM==W, COM==Z, COM==X, COM==Y} 2,
				1 {U1==W, U1==Z, U1==X, U1==Y} 1,
				1 {U2==W, U2==Z, U2==X, U2==Y} 1.
%No pipe, no valve
:- valve(A,B), not symm_pipe(A,B).

%At most Nv valves can be placed
:- valves_number(Nv), not 1 { valve(A,B) } Nv.

%Net must be disconnected from tanks
:- tank(A), symm_pipe(A,B), not valve(A,B).

%1 or 2 valves per edge are allowed
:- valves_per_pipe(N), N==1, valve(A,B), valve(B,A).

%The pipe and its demand must exist 
:- worst_deliv_dem(pipe(A,B),_), not pipe(A,B).
:- worst_deliv_dem(pipe(A,B),D), not dem(A,B,D).


		%23/10/2012
		%
		%Checking the bisection worst sector - not worst
		%At least a valve in between
:- adj(pipe(X,Y), pipe(W,Z), COM, U1, U2), dem(X,Y,D1), not worst_deliv_dem(pipe(X,Y),D1), worst_deliv_dem(pipe(W,Z),_),
	0 { valve(COM,U1), valve(COM,U2) } 0.

		%%
		%% Checking reachability
		%%
		%Among the installed valves, the closed valves for the worst isolation case are those between the fed and the unfed pipes
closed_valve(v(COM,U1)) :- adj(pipe(X,Y), pipe(W,Z), COM, U1, U2), dem(W,Z,D1),
				worst_deliv_dem(pipe(X,Y),_),
				not worst_deliv_dem(pipe(W,Z),D1),
				valve(COM,U1).
closed_valve(v(COM,U2)) :- adj(pipe(X,Y), pipe(W,Z), COM, U1, U2), dem(W,Z,D1),
				worst_deliv_dem(pipe(X,Y),_),
				not worst_deliv_dem(pipe(W,Z),D1),
				valve(COM,U2).
closed_valve(v(A,B)) :- tank(A), pipe(A,B), dem(A,B,D1), not worst_deliv_dem(pipe(A,B),D1).
closed_valve(v(B,A)) :- tank(B), pipe(A,B), dem(A,B,D1), not worst_deliv_dem(pipe(A,B),D1).


		%
		%A pipe adjacent to the tank is reached iff there is no closed valve.
reached(pipe(A,B)):- tank(A), pipe(A,B), not closed_valve(v(A,B)).
reached(pipe(A,B)):- tank(B), pipe(A,B), not closed_valve(v(B,A)).

		%
		%Can we recursively reach any tank??
reached(pipe(A,B)) :- adj(pipe(A,B), pipe(C,D), COM, U1, U2), %COM is not a tank! 
				not closed_valve(v(COM,U1)),
				not closed_valve(v(COM,U2)),
				reached(pipe(C,D)).

		%
		%A pipe in worst_deliv_dem must be reachable!
:- pipe(A,B), worst_deliv_dem(pipe(A,B),_), not reached(pipe(A,B)).

cost(D) :- dem(A,B,D), not worst_deliv_dem(pipe(A,B),D).
costsum(X) :- X=#sum[cost(D)=D].
#hide.
#show costsum/1.
