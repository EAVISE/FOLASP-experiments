#!/bin/bash
# REMEMBER: first argument is a string representing a name for this run.
## timelimit: 0
## memlimit: 0
## output: |  | .dat
## linked_input: |  | .asp
ASPTOOLSDIR=../aspscripts/asptools
cat $ASPTOOLSDIR/aspheader $2 $ASPTOOLSDIR/aspfooter
