#!/bin/bash
ok=false

read answerRow

if [ "$answerRow" == "INCONSISTENT" ] && [ $1 != 20 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT" >&2
	exit 1
fi
if [ "$answerRow" != "INCONSISTENT" ] && [ $1 == 20 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT" >&2
	exit 1
fi
if [ "$answerRow" != "INCOMPLETE" ] && [ $1 != 10 ] && [ $1 != 20 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT" >&2
	exit 1
fi

if [ $1 == 20 ]; then
	echo "DONTKNOW"
        echo "Instances with no solution should not be checked with this checker" >&2
        exit 2
else
	if [ $1 != 10 ]; then
		echo "FAIL"
	        exit 1
	else
		# Use perl script for checking answer set.
		systemResult=$((cat $2 ; echo "$answerRow") | `dirname $0`/checker.pl)
		if [[ $? -ne 0 ]]
		then
    			echo "WARN"
    			echo "Unrecognized input stream or general checker failure." >&2
    			exit 1
		fi

		if [ "$systemResult" == "" ]; then
			ok=false
		else
			ok=true
		fi
	fi
fi
if $ok; then
	echo "OK"
else
	echo "FAIL"
fi 
