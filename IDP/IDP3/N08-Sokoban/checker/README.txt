To use this checker, just execute the checker in a terminal console:
./checker.sh EXITCODE INSTANCE < WITNESS

Given a WITNESS (answer set) as a set of facts on a single line 
the checker will assess whether this represents a witness for an INSTANCE, or not.
Additionally, the EXITCODE of the solver is provided, for details see the specification at

https://www.mat.unical.it/aspcomp2013/ProblemIOSpecification

An example call for the checker would be
	./checker.sh 10 instance.asp < rightSolution.asp
which returns OK on stdout and exits with code 0.

For further diagnostic on wrong witnesses you might want to call

./gringo ./constrWhy.asp instancefile witnessfile | clasp




