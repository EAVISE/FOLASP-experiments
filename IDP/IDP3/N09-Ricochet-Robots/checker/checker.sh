#! /bin/bash
#set -x
cd $(dirname $0)
read answerRow

# Erroneous result/exit code combos
if [ "$answerRow" == "INCONSISTENT" ] && [ $1 != 20 ]; then
    echo "FAIL"
    echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT"
    exit 1
fi
if [ "$answerRow" != "INCONSISTENT" ] && [ $1 == 20 ]; then
    echo "FAIL"
    echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT"
    exit 1
fi
if [ "$answerRow" != "INCOMPLETE" ] && [ $1 != 10 ] && [ $1 != 20 ]; then
    echo "FAIL"
    echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT"
    exit 1
fi

# Negative instances
if [ $1 == 20 ]; then
    if grep -q $(basename $2) unsat.txt; then
	echo "OK"
	exit 0
    else
	echo "FAIL"
	exit 1
    fi
else
    # Positive instances
    if [ $1 == 10 ]; then 
	systemResult=$(echo "$answerRow" |./iclingo-3.0.4 --asp ricochet_chk.asp $2 - 2>&1)
	chkexit=$?
	if [[ $chkexit -eq 10 ]] ; then
	    echo "OK"
	    exit 0
	elif [[ $chkexit -eq 20 ]] ; then
	    echo "FAIL"
	    exit 1
#	elif [[ $chkexit -eq 1 && $systemResult =~ (\s)*ERROR ]] ; then
	elif [[ $chkexit -eq 1 ]] ; then
    	    echo "WARN"
    	    echo "Unrecognized input stream or general checker failure." >&2
    	    exit 1
	fi
    # Uknown exit code
    else
	echo "FAIL"
	exit 1
    fi
fi
