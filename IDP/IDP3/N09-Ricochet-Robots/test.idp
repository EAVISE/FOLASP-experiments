vocabulary V {
  type d isa int
  type dir isa string
  type robot
  type time isa int
  barrier(d,d,dir)
  blocked(time,d,d,d,d)
  dim(d)
  godir(time,dir)
  gorobot(time,robot)
  length(time)
  loc(time,robot,d,d)
  pos(robot,d,d)
  target(time)
  target(robot,d,d)
    l : time
}

theory T : V {
  (! I[time] R1[robot] R2[robot] : (~(gorobot(I,R1) & gorobot(I,R2)) | (R1  =  R2))).
(! D1[dir] D2[dir] I[time] : (~(godir(I,D1) & godir(I,D2)) | (D1  =  D2))).
(! I[time] R[robot] X1[d] X2[d] Y1[d] Y2[d] : (~(loc(I,R,X1,Y1) & loc(I,R,X2,Y2)) | ((X1  =  X2) & (Y1  =  Y2)))).
(! I[time] R1[robot] R2[robot] X[d] Y[d] : (~(loc(I,R1,X,Y) & loc(I,R2,X,Y)) | (R1  =  R2))).
(! I[time] R[robot] : (? X[d] Y[d] : loc(I,R,X,Y))).
(! I[time] : (~length(I) | target(I))).
(! I[time] : (~target(I) | (! R[robot] : ~gorobot(I,R)))).
(! I[time] : (target(I) | (? R[robot] : gorobot(I,R)))).
(! I[time] R[robot] X1[d] X2[d] Y1[d] Y2[d] : (~(gorobot(I,R) & (loc(I,R,X1,Y1) & loc(+(I,1),R,X2,Y2))) | ((X1  ~=  X2) | (Y1  ~=  Y2)))).
  {
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((X  =  +(Z,1)) & ((Y  =  W) & (barrier(X,Y,"west") | barrier(Z,W,"east")))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((Z  =  +(X,1)) & ((Y  =  W) & (barrier(X,Y,"east") | barrier(Z,W,"west")))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((X  =  Z) & ((Y  =  +(W,1)) & (barrier(X,Y,"north") | barrier(Z,W,"south")))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((X  =  Z) & ((W  =  +(Y,1)) & (barrier(X,Y,"south") | barrier(Z,W,"north")))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((X  <  Z) & ((Y  =  W) & (? X1[d] X2[d] : ((X  =<  X1) & ((X1  <  X2) & ((X2  =<  Z) & blocked(I,X1,Y,X2,Y))))))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((Z  <  X) & ((Y  =  W) & (? X1[d] X2[d] : ((Z  =<  X1) & ((X1  <  X2) & ((X2  =<  X) & blocked(I,X2,Y,X1,Y))))))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((X  =  Z) & ((Y  <  W) & (? Y1[d] Y2[d] : ((Y  =<  Y1) & ((Y1  <  Y2) & ((Y2  =<  W) & blocked(I,X,Y1,X,Y2))))))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((X  =  Z) & ((Y  >  W) & (? Y1[d] Y2[d] : ((W  =<  Y1) & ((Y1  <  Y2) & ((Y2  =<  Y) & blocked(I,X,Y2,X,Y1))))))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((X  <  Z) & ((Y  =  W) & (? X1[d] : ((X  <  X1) & ((X1  =<  Z) & (? R[robot] : loc(I,R,X1,Y))))))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((Z  <  X) & ((Y  =  W) & (? X1[d] : ((Z  =<  X1) & ((X1  <  X) & (? R[robot] : loc(I,R,X1,Y))))))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((X  =  Z) & ((Y  <  W) & (? Y1[d] : ((Y  <  Y1) & ((Y1  =<  W) & (? R[robot] : loc(I,R,X,Y1))))))).
    ! I[time] W[d] X[d] Y[d] Z[d] : blocked(I,X,Y,Z,W) <- ((X  =  Z) & ((Y  >  W) & (? Y1[d] : ((W  =<  Y1) & ((Y1  <  Y) & (? R[robot] : loc(I,R,X,Y1))))))).
  }
  {
    ! T[time] : target(T) <- (! R[robot] X[d] Y[d] : (~target(R,X,Y) | loc(T,R,X,Y))).
  }
  {
    ! R[robot] X[d] Y[d] : loc(0,R,X,Y) <- (pos(R,X,Y) & time(0)).
    ! I[time] R[robot] X[d] Y[d] : loc(I,R,X,Y) <- ((I  =<  l) & (loc(-(I,1),R,X,Y) & ~gorobot(-(I,1),R))).
    ! I[time] R[robot] X[d] Y[d] : loc(I,R,X,Y) <- ((I  =<  l) & (? Y1[d] : (loc(-(I,1),R,X,Y1) & ((Y1  <  Y) & (gorobot(-(I,1),R) & (godir(-(I,1),"south") & (~blocked(-(I,1),X,Y1,X,Y) & (blocked(-(I,1),X,Y,X,+(Y,1)) | ~dim(+(Y,1)))))))))).
    ! I[time] R[robot] X[d] Y[d] : loc(I,R,X,Y) <- ((I  =<  l) & (? Y1[d] : (loc(-(I,1),R,X,Y1) & ((Y  <  Y1) & (gorobot(-(I,1),R) & (godir(-(I,1),"north") & (~blocked(-(I,1),X,Y1,X,Y) & (blocked(-(I,1),X,Y,X,-(Y,1)) | ~dim(-(Y,1)))))))))).
    ! I[time] R[robot] X[d] Y[d] : loc(I,R,X,Y) <- ((I  =<  l) & (? X1[d] : (loc(-(I,1),R,X1,Y) & ((X1  <  X) & (gorobot(-(I,1),R) & (godir(-(I,1),"east") & (~blocked(-(I,1),X1,Y,X,Y) & (blocked(-(I,1),X,Y,+(X,1),Y) | ~dim(+(X,1)))))))))).
    ! I[time] R[robot] X[d] Y[d] : loc(I,R,X,Y) <- ((I  =<  l) & (? X1[d] : (loc(-(I,1),R,X1,Y) & ((X  <  X1) & (gorobot(-(I,1),R) & (godir(-(I,1),"west") & (~blocked(-(I,1),X1,Y,X,Y) & (blocked(-(I,1),X,Y,-(X,1),Y) | ~dim(-(X,1)))))))))).
  }
}

structure S : V {
  d = { 1..5 }
  dir = { "east"; "north"; "south"; "west" }
  robot = { "blue"; "green"; "red"; "yellow" }
  time = { 0..5 }
  barrier = { 1,2,"south"; 2,5,"east"; 4,3,"east" }
  dim = { 1; 2; 3; 4; 5 }
  length = { 5 }
  pos = { "blue",1,5; "green",5,1; "red",1,1; "yellow",5,5 }
  target = { "red",4,4 }
  l = 5
}

procedure main(){
printmodels(modelexpand(T,S))
}
