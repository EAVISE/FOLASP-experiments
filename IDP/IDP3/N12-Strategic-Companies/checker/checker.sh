#! /bin/bash
ok=false

read answerRow

if [ "$answerRow" == "no." ] && [ $1 != 20 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT 1 " >&2
	exit 1
fi
if [ "$answerRow" == "yes." ] && [ $1 != 10 ]; then
	echo "FAIL"
	echo "MISMATCH OF SOLVER RETURN VALUE AND STANDARD INPUT 2" >&2
	exit 1
fi

`dirname $0`/check_answer.py $answerRow `basename $2`

