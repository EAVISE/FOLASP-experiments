/**
 * Title: A new IDP source file
 * Author: jodv
 */
procedure main(){
    stdoptions.cpsupport = true
    stdoptions.verbosity.grounding = 1
    stdoptions.verbosity.solving = 1
    
    setvocabulary(struc,mergedVoc)
    setvocabulary(S,mergedVoc)    
    newStruc=merge(struc,S)
    setvocabulary(newStruc,fullVoc)
    
    result=modelexpand(T,newStruc)[1]
    
    if result==nil then
        print("INCONSISTENT")
        return 20
    else
    		setvocabulary(result,outVoc)
    		sol=modelexpand(Out,result)[1]
        printfacts("move",sol[outVoc::move].ct)
        return 10
    end
}

vocabulary input{
	type Coord isa int
	type Time isa int
	
	time(Time)
	full(Coord,Coord)
	empty(Coord,Coord)
}

vocabulary InterpretedVoc{
    type Dir
    type Adjustment isa int
    
    OffX(Dir):Adjustment
    OffY(Dir):Adjustment
}

vocabulary mergedVoc{
	extern vocabulary input
	extern vocabulary InterpretedVoc 
}

vocabulary fullVoc{
	extern vocabulary mergedVoc
	
	TermFull(Time,Coord,Coord)
	InitFull(Time,Coord,Coord)
	Direction(Time):Dir
	MoveX(Time):Coord
	MoveY(Time):Coord
	full(Time,Coord,Coord)
	Square(Coord,Coord)
}

vocabulary outVoc{
	extern vocabulary fullVoc
	move(Time,Dir,Coord,Coord)
}

theory T:fullVoc{
	
	// initialize full:
	!x y: full(1,x,y) <=> full(x,y).
	// initialize Square:
	{ Square(c,r) <- full(c,r) | empty(c,r). }
	
// don't jump into forbidden squares
	!t: Square(MoveX(t),MoveY(t)).
	!t: Square(MoveX(t)+2*OffX(Direction(t)),MoveY(t)+2*OffY(Direction(t))).
	
	// only move allowed moves:
	! t: ?x: x=MoveX(t) & ?y: y=MoveY(t) & ?d: d=Direction(t) & 
		full(t,x,y) & 
		full(t,x+OffX(d),y+OffY(d)) & 
		~full(t,x+2*OffX(d),y+2*OffY(d)).
--	! t: full(t,MoveX(t),MoveY(t)).
--	! t: full(t,MoveX(t)+OffX(Direction(t)),MoveY(t)+OffY(Direction(t))).
--	! t: ~full(t,MoveX(t)+2*OffX(Direction(t)),MoveY(t)+2*OffY(Direction(t))).

	{
		TermFull(t,x,y) <- x=MoveX(t) & y=MoveY(t).
		TermFull(t,x,y) <- d=Direction(t) & x=MoveX(t)+OffX(d) & y=MoveY(t)+OffY(d).
	}
	
	{
		InitFull(t,x,y) <- d=Direction(t) & x=MoveX(t)+2*OffX(d) & y=MoveY(t)+2*OffY(d). 
	}
	
	! t x y: Time(t+1) => ((full(t,x,y) & ~full(t+1,x,y)) <=>  TermFull(t,x,y)).
	! t x y: Time(t+1) => ((~full(t,x,y) & full(t+1,x,y)) <=>  InitFull(t,x,y)).
	
	// TODO: check whether this is a superfluous constraint
	! t x y: Time(t+1) => ((full(t,x,y) <=> full(t+1,x,y)) <=>  (~TermFull(t,x,y) & ~InitFull(t,x,y))).
	
	
}

theory Out:outVoc{
	{
		move(t,d,x,y) <- d=Direction(t) & x=MoveX(t) & y=MoveY(t).
	}
}

structure struc:InterpretedVoc{
	Dir={up;down;left;right}
	OffX={up->0;down->0;left->-1;right->1}
	OffY={up->-1;down->1;left->0;right->0}
	Adjustment={-1;0;1}
}
/* 
        x                       
        1   2   3   4   5   6   7
y   1           x   x   x       
    2           x   x   x       
    3   x   x   x   x   x   x   x
    4   x   x   x   x   x   x   x
    5   x   x   x   x   x   x   x
    6           x   x   x       
    7           x   x   x
*/     