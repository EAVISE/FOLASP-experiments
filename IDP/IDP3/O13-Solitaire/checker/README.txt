To use this checker, just execute the checker in a terminal console:
./checker.sh EXITCODE INSTANCE < WITNESS

Given a WITNESS (answer set) as a set of facts on a single line 
the checker will assess whether WITNESS+EXITCODE represent a 
correct solution for INSTANCE. For details see the specification at:

https://www.mat.unical.it/aspcomp2013/ProblemIOSpecification

An example call for the checker would be
	./checker.sh 10 instance.asp < rightSolution.asp
which returns OK on stdout and exits with code 0.
