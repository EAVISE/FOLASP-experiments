include "procedures.idp"

/** INPUT AND TRANSLATION **/

vocabulary inputTypes{
	type leaf 
	type weight isa int
	type cardi isa int

	type mwt isa int//useless but for avoiding int
}

vocabulary usefulInput{
	extern vocabulary inputTypes
	leafWeightCardinality(leaf,weight,cardi)
	max_total_weight(mwt)
}

vocabulary uselessInput{
	extern vocabulary inputTypes
	num(int) 
	innerNode(int) 
}

vocabulary input{
	extern vocabulary usefulInput
	extern vocabulary uselessInput
}

vocabulary translatedInput{
	extern vocabulary inputTypes
	weightOf(leaf) : weight
	cardOf(leaf) : cardi
	max_weight:mwt
}

vocabulary translationVoc{
	extern vocabulary usefulInput
	extern vocabulary translatedInput
}

theory translationTheo : translationVoc{
	{
		weightOf(l) = w <- leafWeightCardinality(l,w,c).
		cardOf(l) = c <- leafWeightCardinality(l,w,c).
		max_weight = w <- max_total_weight(w).
	}
}

/** EXTRA INPUT **/
vocabulary extraInput{
	type color isa string
	type position isa int
	type possibleCost isa int
}

structure extraStruc: extraInput{
	color={"none";"red";"blue";"green"}
}

/** PROBLEM SPECIFICATION **/
vocabulary problemInput{
	extern vocabulary translatedInput
	extern vocabulary extraInput
}

vocabulary problemVoc{
	extern vocabulary problemInput
	//NEEDED FOR COMPETITION
	leafPos(leaf) : position 

	//EXTRA
	colorOf(leaf):color

	neighbours(leaf,leaf)

	costOf(leaf) : possibleCost
	
}

theory solution : problemVoc{
	//Only the first leaf is uncolored
	!l : colorOf(l) = "none" <=> leafPos(l) = 0.
	
	//Cost of leaves can be calculated in terms of their color.
	!l : colorOf(l) = "none" => costOf(l) = weightOf(l).
	!l : colorOf(l) = "green" => costOf(l) = weightOf(l) + cardOf(l).
	!l l': colorOf(l) = "red" &  neighbours(l',l) => costOf(l) = costOf(l') + weightOf(l).
	!l l': colorOf(l) = "blue" & neighbours(l',l) => costOf(l) = costOf(l') + cardOf(l).
	
	//shared tseitin
	!l l': neighbours(l',l) <=>  leafPos(l') = leafPos(l) - 1.

	//TOTAL COST IS SMALL ENOUGH
	sum{l:colorOf(l) ~= "none":costOf(l)} =< max_weight.

	//leafpos is a bijection
	!p: #{l: leafPos(l)=p} = 1.


	//REDUNDANT CONSTRAINTS

	//You can always color a node green. If this reduces its cost, do so!
	!l: costOf(l) =< weightOf(l) + cardOf(l).

	//cost has minimum value
	!l: costOf(l) >= weightOf(l) | costOf(l) >= cardOf(l).

	//Can we choose first place in advance?

	
	

	
}

vocabulary postprocessVoc{
	extern vocabulary problemVoc
	partial posColor(position): color
	leafAt(position):leaf

}

theory postprocessTheo : postprocessVoc{
//leafAt and leafPos are each other's inverse
	{
		! l p : leafAt(p) = l <- leafPos(l) = p.
	}

	{
		posColor(p) = c <- p>0 & colorOf(leafAt(p)) = c.
	}
}

