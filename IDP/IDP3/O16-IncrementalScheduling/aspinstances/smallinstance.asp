max_value(20).


job(j1).
job(j2).
job(j3).

device(d1).
device(d2).

instances(d1,1).
instances(d2,2).

offline_instance(d2,1).

job_device(j1,d1).
job_device(j2,d2).
job_device(j3,d2).

job_len(j1,4).
job_len(j2,5).
job_len(j3,4).

precedes(j1,j2).

deadline(j2,10).
deadline(j3,12).

importance(j2,1).
importance(j3,2).

max_total_penalty(2).

curr_job_start(j1,0).
curr_job_start(j2,4).

curr_on_instance(j1,1).
curr_on_instance(j2,1).
curr_time(2).