% When using encoding.asp with gringo or clingo the following warning may be output:
       % warning: checking_solution/0 is never defined
% The message can be safely disregarded.
max_value(20).
device(d1). instances(d1,1).
device(d2). instances(d2,2).
offline_instance(d2,1).
%
job(j1). job_device(j1,d1). job_len(j1,4).
job(j2). job_device(j2,d2). job_len(j2,5). deadline(j2,10). importance(j2,1).
precedes(j1,j2).
job(j3). job_device(j3,d2). job_len(j3,4). deadline(j3,12). importance(j3,2).
%
max_total_penalty(3).
%
curr_job_start(j1,0). curr_on_instance(j1,1).
curr_job_start(j2,4). curr_on_instance(j2,1).
%
curr_time(2).
