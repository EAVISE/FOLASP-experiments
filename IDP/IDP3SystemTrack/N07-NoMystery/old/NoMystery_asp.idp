vocabulary input{
    type loc
    type object
    type truck isa object
 	type pack  isa object
    type step isa int

// input data
   	at(object,loc)
	fuel(truck,int)
    fuelcost(int,loc,loc)
    goal(pack,loc)

// Output:  
    load(pack,truck,loc,step)
    unload(pack,truck,loc,step)
	drive(truck,loc,loc,step)

// extra
	package(pack)
	location(loc)
	locatable(object)
	at(object,loc,step)
    in(pack,truck,step)
    fuel(truck,int,step)
    delat(object,loc,step)
	delin(pack,truck,step)
	delfuel(truck,int,step)
	preconditions_u(pack,truck,loc,step)
	preconditions_l(pack,truck,loc,step)
	preconditions_d(truck,loc,loc,step )
	goalreached
}


theory T:input{  

// truck(T) :- fuel(T,_).
// package(P) :- at(P,L), not truck(P).
// location(L) :- fuelcost(_,L,_).
// location(L) :- fuelcost(_,_,L).
// locatable(O) :- at(O,L).

// de "types"
{ truck(T) <- fuel(T,f).
  package(P) <- at(P,L) & ~truck(P).
  location(L) <- fuelcost(c,L,l).
  location(L) <- fuelcost(c,l,L).
  locatable(O) <- at(O,L).
}

// %
// at(O,L,0) :- at(O,L).
// fuel(T,F,0) :- fuel(T,F).
// %

{ at(O,L,0) <- at(O,L).
  fuel(T,F,0) <- fuel(T,F).
}

// % 
// % GENERATE  >>>>>
// 1 <= { unload( P,T,L,S ) : 
//        package( P ) , 
 // 	truck( T ) , 
// 	location( L ); 
 //    load( P,T,L,S ) : 
// 	package( P ) , 
// 	truck( T ) , 
// 	location( L ); 
 //    drive( T,L1,L2,S ) : 
// 	fuelcost( Fueldelta,L1,L2 ) , 
// 	truck( T );
 //    noop(S)
//   } <= 1 :- step(S), S > 0.
// % <<<<<  GENERATE

// translates in well-typed constraints 
unload( P,T,L,S ) => package( P ) & truck( T ) & location( L ) &
		     step(S) & S > 0.
load( P,T,L,S ) => package( P ) & truck( T ) & location( L ) &
		     step(S) & S > 0.
drive( T,L1,L2,S ) => ?Fueldelta: fuelcost( Fueldelta,L1,L2 ) & truck( T ) &
		     step(S) & S > 0.

// % unload/4, effects
// at( P,L,S ) :- unload( P,T,L,S ).
// del( in( P,T ),S ) :- unload( P,T,L,S ).
{
at( P,L,S ) <- unload( P,T,L,S ).
delin( P,T,S ) <- unload( P,T,L,S ).


// % load/4, effects
// del( at( P,L ),S ) :- load( P,T,L,S ).
// in( P,T,S ) :- load( P,T,L,S ).

delat( P,L ,S ) <- load( P,T,L,S ).
in( P,T,S ) <- load( P,T,L,S ).

// % drive/4, effects
// del( at( T,L1 ), S ) :- drive( T,L1,L2,S ).
// at( T,L2,S ) :- drive( T,L1,L2,S). 
// del( fuel( T,Fuelpre ),S ) :- drive( T,L1,L2,S ), fuel(T, Fuelpre,S-1).
// fuel( T,Fuelpost,S ) :- drive( T,L1,L2,S ), fuelcost(Fueldelta,L1,L2), fuel(T,Fuelpre,S-1), Fuelpost = Fuelpre - Fueldelta.

delat( T,L1 , S ) <- drive( T,L1,L2,S ).
at( T,L2,S ) <- drive( T,L1,L2,S).
delfuel( T,Fuelpre,S ) <- drive( T,L1,L2,S ) & fuel(T, Fuelpre,S-1).
fuel( T,Fuelpost,S ) <- drive( T,L1,L2,S ) & fuelcost(Fueldelta,L1,L2)
            & fuel(T,Fuelpre,S-1) & Fuelpost = Fuelpre - Fueldelta. 


// % <<<<<  EFFECTS APPLY
// % 
// % INERTIA  >>>>>
// at( O,L,S ) :- at( O,L,S-1 ), not del( at( O,L ),S  ), step(S).
// in( P,T,S ) :- in( P,T,S-1 ), not del( in( P,T ),S  ), step(S).
// fuel( T,Level,S ) :- fuel( T,Level,S-1 ), not del( fuel( T,Level) ,S ), truck( T ), step(S).
// % <<<<<  INERTIA

at( O,L,S ) <- at( O,L,S-1 ) & ~delat( O,L ,S  ) & step(S).
in( P,T,S ) <- in( P,T,S-1 ) & ~delin( P,T ,S  ) & step(S).
fuel( T,Level,S ) <- fuel( T,Level,S-1 ) & ~delfuel( T,Level ,S ) & 
                     truck( T ) & step(S).
}

// % 
// % 
// % PRECONDITIONS CHECK  >>>>>

{preconditions_u( P,T,L,S ) <- step(S) & at( T,L,S-1 ) & in( P,T,S-1 )
                             & package( P ) & truck( T ).

preconditions_l( P,T,L,S ) <- step(S) & at( T,L,S-1 ) & at( P,L,S-1 ).

preconditions_d( T,L1,L2,S ) <- step(S) & at( T,L1,S-1 ) & 
                               fuel( T, Fuelpre, S-1) & 
                     fuelcost(Fueldelta,L1,L2) & Fuelpre - Fueldelta >= 0.
}

// % unload/4, preconditions
//  :- unload( P,T,L,S ), not preconditions_u( P,T,L,S ).
// preconditions_u( P,T,L,S ) :- step(S), at( T,L,S-1 ), in( P,T,S-1 ), package( P ), truck( T ).

~(unload( P,T,L,S ) & ~preconditions_u( P,T,L,S ) ).


// % load/4, preconditions
//  :- load( P,T,L,S ), not preconditions_l( P,T,L,S ).
// preconditions_l( P,T,L,S ) :- step(S), at( T,L,S-1 ), at( P,L,S-1 ).

~(load( P,T,L,S ) & ~preconditions_l( P,T,L,S ) ).

// % drive/5, preconditions
//  :- drive( T,L1,L2,S ), not preconditions_d( T,L1,L2,S ).
// preconditions_d( T,L1,L2,S ) :- step(S), at( T,L1,S-1 ), fuel( T, Fuelpre, S-1), fuelcost(Fueldelta,L1,L2), Fuelpre - Fueldelta >= 0.

~(drive( T,L1,L2,S ) & ~preconditions_d( T,L1,L2,S ) ).

// % <<<<<  PRECONDITIONS HOLD
// % 

// % GOAL CHECK
// goalreached :- step(S),  N = #count{ P,L,S : at(P,L,S) , goal(P,L) }, N = #count{ P,L : goal(P,L) }.
// :- not goalreached.

{goalreached <- step(S) &  #{ P L : at(P,L,S) & goal(P,L) }=#{ P L : goal(P,L) }.
}

goalreached.
}

procedure main(){
--    stdoptions.cpsupport = true
    stdoptions.verbosity.grounding = 2
    stdoptions.verbosity.solving = 1
/*
	setvocabulary(S,symbols)
	fullstruc=merge(S,symbolInter)
	print(fullstruc)
*/
    result=modelexpand(T,S)[1]
    
    if result==nil then
        print("INCONSISTENT")
        return 20
    else
    	print(result)  
        printfacts("move",result[input::move].ct)
        return 10
    end
}

