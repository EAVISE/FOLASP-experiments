#!/bin/bash

declare -a problems=(
"N01-Permutation-Pattern-Matching"
"N02-ValvesLocationProblem"
"N04-Connected-Density-Maximum-Still-life"
"N05-Graceful-Graphs"
"N06-Bottle-Filling"
"N07-Nomystery"
"N08-Sokoban"
"N09-Ricochet-Robots"
"O10-Crossing-Minimization"
"O11-Reachability"
"O13-Solitaire"
"O14-WeightedSequenceProblem"
"O15-StableMarriage"
"O16-IncrementalScheduling"
)

declare -a instances=(
"/home/jod/workspace/FOLASP-experiments/instances/t01/b01"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b02"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b04"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b05"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b06"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b07"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b08"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b09"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b10"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b11"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b13"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b14"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b15"
"/home/jod/workspace/FOLASP-experiments/instances/t01/b16"
)

for i in "${!problems[@]}"; do
  prob=${problems[i]}
  inst=${instances[i]}
  echo $prob
  cd IDP3/$prob
  for f in $inst/instances/*.asp ; do
    out="$(basename $f)"
    ./1_solve.sh . <(./0_cat_asp.sh . $f) > /home/jod/workspace/FOLASP-experiments/folasp-experiments/dockervolume/instances/$prob/$out.idp
  done
  cd ../..
done
