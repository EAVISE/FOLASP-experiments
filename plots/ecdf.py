#!/usr/bin/python3

import csv
import sys
import numpy as np
import matplotlib.pyplot as plt

extensions=["svg","pdf","png"]
graphpath = "/home/jod/workspace/folasp-experiments/plots"

timeout = 3000
memout = 32000
csvs = []

def readCSV(filename,csvs):
    with open(filename, newline='') as csvfile:
        tmp = []
        tmp.extend(csv.reader(csvfile, delimiter=';', quotechar='\"'))
        csvs += [tmp]

for i in range(1,len(sys.argv)):
    readCSV(sys.argv[i],csvs)

nbinstances = len(csvs[0])-1
rescol = 2
timecol1 = 3
timecol2 = 4
memcol = 5

def timeFromLine(line):
    return float(line[timecol1])+float(line[timecol2])

def memFromLine(line):
    return float(line[memcol])/1000

def success(line):
    return int(line[rescol])==0 and memFromLine(line)<memout and timeFromLine(line)<timeout

def plot(fromLine,out,x_axis_legend,title):
    times = [ sorted([fromLine(line) for line in data[1:] if success(line)]) for data in csvs]

    fig, ax = plt.subplots(figsize=(5,5))
    plt.title(title+" ECDF\n (higher is better, "+str(nbinstances)+" instances)")
    plt.xlabel(x_axis_legend+" (max "+str(out)+")")
    plt.ylabel('Number of solved instances')
    for t in times:
        ax.plot(t+[2*out]*(nbinstances-len(t)), range(1,nbinstances+1))
    x_legend = []
    for i in range (0,len(times)):
        x_legend+=[sys.argv[i+1]+" ("+str(len(times[i]))+")"]
    ax.legend(x_legend)
    ax.set_xscale('log')
    ax.set_xlim([1,out])
    ax.set_ylim([0,nbinstances])
    for extension in extensions:
        plt.savefig(graphpath+'/ecdf_'+title+'.'+extension,filetype=extension,bbox_inches="tight")
    plt.close(fig)

plot(timeFromLine,timeout,'Time limit (s)','Time')
plot(memFromLine,memout,'Memory limit (MB)','Memory')

