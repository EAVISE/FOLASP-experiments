#!/usr/bin/python3

import csv
import sys
import numpy as np
import matplotlib.pyplot as plt
import os
from shared import *

extensions=["svg","pdf","png"]
graphpath = os.path.realpath(__file__)
graphpath = graphpath[:graphpath.rfind('/')+1]

csvs = []
runs = []

def readCSV(filename,csvs):
    with open(filename, newline='') as csvfile:
        tmp = []
        tmp.extend(csv.reader(csvfile, delimiter=';', quotechar='\"'))
        csvs += [sorted(tmp[1:])]

for c in sys.argv[1:]:
    readCSV(c,csvs)

runs = [x[:-4] for x in sys.argv[1:]]
print(runs)
nbinstances = len(csvs[0])

def plot(fromLine,base,out,title,onlysuccess):
    fig, ax = plt.subplots(figsize=(5,5))
    plt.title(title+" (max "+str(out)+") ("+str(nbinstances)+" instances)")
    plt.xlabel(runs[0])
    plt.ylabel(runs[1])
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim([base,out])
    ax.set_ylim([base,out])
    
    groups = sorted({ getFamily(line) for line in csvs[0] })
    legend = []
    
    for group in groups:
        print(getMarker(group))
        times = [ [fromLine(line) for line in data if group==getFamily(line)] for data in csvs]
        pairs = [x for x in zip(times[0],times[1]) if (not onlysuccess or (x[0]<out and x[1]<out))]
        ax.scatter([x[0] for x in pairs],[x[1] for x in pairs],alpha=0.5, marker=getMarker(group), color=getColor(group), edgecolors=None)
        legend+=[group+" ("+ str(len([x for x in pairs if x[0]<out]))+" vs "+str(len([x for x in pairs if x[1]<out]))+ ")"]
        
    ax.legend(legend,bbox_to_anchor=(1, 1))
        

    for extension in extensions:
        plt.savefig(graphpath+'/plots/scatter_'+title.replace(" ", "_")+'_'+runs[0]+'_'+runs[1]+'.'+extension,filetype=extension,bbox_inches="tight")
    plt.close(fig)

plot(timeFromLine,mintime,timeout,'Time (s)',False)
# plot(memFromLine,minmem,memout,'Memory (MB)',True)
plot(groundsizeFromLine,minground,maxground,'Ground size (atoms)',False)

