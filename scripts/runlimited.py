from resource import *
import time
import subprocess
import sys

timeout = int(sys.argv[1]) # seconds
memout = int(sys.argv[2])*1000000 # MB
safety = 1.1

outdir = sys.argv[3]
purehash = sys.argv[4]

filename = outdir+'/'+purehash
metafile = filename+'.meta'
outfile = filename+'.out'
errfile = filename+'.err'

command = sys.argv[5:]

outcode = 0

setrlimit(RLIMIT_CPU,(timeout,int(timeout*safety)))
setrlimit(RLIMIT_AS,(memout,int(memout*safety)))

output = ""
error = ""

try:
    tmp = subprocess.check_output(command)
    usage = getrusage(RUSAGE_CHILDREN)
    output = str(tmp,"utf-8")
except subprocess.CalledProcessError as e:
    usage = getrusage(RUSAGE_CHILDREN)
    if e.output: output = str(e.output,"utf-8")
    if e.stderr: error = str(e.stderr,"utf-8")
    outcode = e.returncode

with open(metafile, 'w') as f:
    f.write("#!/usr/bin/env bash\n\n")
    for c in command:
        f.write(c)
        f.write(" ")
    f.write("\n\n")
    f.write("## hash "+str(purehash)+"\n")
    f.write("## outcode "+str(outcode)+"\n")
    f.write("## usertime "+str(usage.ru_utime)+"\n")
    f.write("## systemtime "+str(usage.ru_stime)+"\n")
    f.write("## memory "+str(usage.ru_maxrss)+"\n")
    
with open(outfile, 'w') as f:
    f.write(output)
    
with open(errfile, 'w') as f:
    f.write(error)

